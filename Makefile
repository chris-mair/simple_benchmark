all:
	c++ -std=c++11 -O3 -Wall memcopy.cpp -o memcopy
	c++ -std=c++11 -O3 -Wall matmul.cpp -o matmul -l openblas -I /opt/local/include/openblas -I /opt/homebrew/include -L /opt/local/lib/ -L /opt/homebrew/lib
