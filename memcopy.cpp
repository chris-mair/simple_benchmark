/*
  simple_benchmark - memcopy.cpp
*/

#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <random>

#include <cstdlib>
#include <cstring>

typedef std::chrono::steady_clock clk;

static void copy_while(uint64_t *src_start, const uint64_t *src_end, uint64_t *dst_start, int rep) {
    uint64_t *src, *dst;
    for (int r = 0; r < rep; r++) {
        src = src_start;
        dst = dst_start;
        while (src < src_end) {
            *(dst++) = *(src++);
        }
    }
}

static void copy_memcpy(const uint64_t *src_start, const uint64_t *src_end, uint64_t *dst_start, int rep) {
    for (int r = 0; r < rep; r++) {
        memcpy(dst_start, src_start, (src_end - src_start) * sizeof(uint64_t));
    }
}

int main() {

    const uint64_t NUM = 1024*1024*1024;    // number of uint64 values to copy
    const uint64_t REP = 10;                // number of repetitions
    const uint64_t THREAD_MAX = 6;          // maximum number of threads to use

    uint64_t *block_a, *block_b;
    uint64_t *src, *src_start, *src_end, *dst_start;

    uint64_t value, sum, checksum, total_size;
    std::random_device rd;
    std::mt19937_64 eng(rd());
    std::uniform_int_distribution<uint64_t> flat_rnd;

    clk::time_point t0, t1;

    std::cout << std::fixed << std::setprecision(3);

    block_a = (uint64_t *)malloc(NUM * sizeof(uint64_t));
    block_b = (uint64_t *)malloc(NUM * sizeof(uint64_t));
    if (block_a == nullptr || block_b == nullptr) {
        std::cerr << "*** malloc failed\n";
        return 1;
    }
    std::cout << "mem: two blocks of " << NUM << " numbers (" << (NUM * sizeof(uint64_t)) << " bytes) each\n";
    std::cout << "block a => [" << block_a << ", " << block_a + NUM << ")\n";
    std::cout << "block b => [" << block_b << ", " << block_b + NUM << ")\n";

    std::cout << "tick = " << clk::period::num << "/" << clk::period::den << " s\n";
    double tick = (double) clk::period::num / (double) clk::period::den;

    std::cout << "repetitions = " << REP << "\n";

    src = block_a;
    src_end = block_a + NUM;
    checksum = 0;
    while (src < src_end) {
        value = flat_rnd(eng);
        *(src++) = value;
        checksum += value;
    }

    std::cout << std::setw(3) << "threads"
              << std::setw(12) << "method"
              << std::setw(9) << "GiB/s"
              << "\n";

    for (bool use_while : {true, false}) {

        for (unsigned int t_num = 1; t_num <= THREAD_MAX; t_num++) {

            std::thread pool[t_num];

            memset(block_b, 0, NUM * sizeof(uint64_t));

            t0 = clk::now();
            total_size = 0;
            for (unsigned int t = 1; t <= t_num; t++) {
                src_start = block_a + (t - 1) * NUM / t_num;
                src_end = block_a + t * NUM / t_num;
                dst_start = block_b + (t - 1) * NUM / t_num;
                total_size += src_end - src_start;
                if (use_while) {
                    pool[t - 1] = std::thread(copy_while, src_start, src_end, dst_start, REP);
                } else {
                    pool[t - 1] = std::thread(copy_memcpy, src_start, src_end, dst_start, REP);
                }
            }
            if (NUM != total_size) {
                std::cerr << "*** wrong check size\n";
                return 1;
            }

            for (unsigned int t = 1; t <= t_num; t++) {
                pool[t - 1].join();
            }

            t1 = clk::now();

            std::cout << std::setw(7) << t_num
                      << std::setw(12) << (use_while ? "while loop" : "memcpy")
                      << std::setw(9)
                      << (double) (REP * NUM * sizeof(uint64_t)) / ((double) (t1 - t0).count() * tick) / 1024.0 /
                         1024.0 / 1024.0
                      << "\n";

            src = block_b;
            src_end = block_b + NUM;
            sum = 0;
            while (src < src_end) {
                sum += *(src++);
            }

            if (sum != checksum) {
                std::cerr << "*** wrong check sum\n";
                return 1;
            }

        }

    }

    free(block_b);
    free(block_a);

    return 0;
}
