## Simple Benchmark Tools

This project implements a few simple, synthetic benchmarks in C++.

Currently, there is just one :)

I plan to add more in the future!

### Building

This should build just fine on any system with a C++ 11 compiler.

Compile with:

```text
c++ -std=c++11 -O3 -Wall memcopy.cpp -o memcopy
```

Note: on older Linux distributions you might get a linker error complaining about
an undefined reference to `pthread_create`. To solve this, just add a ` -lpthread`
to the `c++` line.

### memcopy - test memory bandwidth

**What it does**

Copy a large block of memory and output copy performance in GiB/s.

**How to run it**

Noting that 16 GiB of free RAM is needed, just run:

```text
./memcopy
```

**Details**

The default block size is 8 GiB. Therefore, 16 GiB of free memory is needed.
Change `const NUM` in `main` to change the block size.

Two different methods are used to copy the block: 

- "while loop" uses a while loop to copy uint64 values
- "memcpy" uses the `memcpy` function from the standard library

The copy is performed using 1 to 6 threads, where each thread copies
an equal part of the block concurrently.

Each copy operation is repeated 10 times.

Here are some results for machines of different classes.

| machine                                                  | OS and compiler            | max memory copy performance                                              | note |
|----------------------------------------------------------|----------------------------|--------------------------------------------------------------------------|------|
| Notebook, Apple M2 Pro,<br>6+4 cores, 32 GB RAM          | macOS 13.6, clang 15.0     | 81.4 GiB/s at 4 threads for while,<br>81.8 GiB/s at 4 threads for memcpy |      |
| PC, Ryzen 9 7950X,<br>16 cores, 128 GB DDR5-3600 ECC-RAM | Debian 12.5, GNU C++ 12.2  | 15.1 GiB/s at 2 threads for while,<br>21.8 GiB/s at 4 threads for memcpy |      | 
| EC2 VM, r6a.xlarge with EPYC 7R13,<br>4 vCPUs, 32 GB ECC-RAM | Ubuntu 22.04, GNU C++ 11.4 | 14.8 GiB at 4 threads for while,<br>20.1 GiB at 4 threads for memcpy     | [^1] | 
| SBC, Raspberry Pi 4B<br>4 cores, 4GB RAM                 | Raspian 10.13, GNU C++ 8.3 | 1.7 GiB at 1 thread for while,<br>1.7 GiB at 1 thread for memcpy         | [^2] |

[1] shared hardware  
[2] block size lowered to 1 GiB, 32-bit OS
