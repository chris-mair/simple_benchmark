/*

  note we need: libopenblas-dev

  test multi-thread performance:

      ./matmul

  test single-thread performance:

      OPENBLAS_NUM_THREADS=1 ./matmul

*/

#include <iostream>
#include <iomanip>
#include <chrono>

#include <cblas.h>

typedef std::chrono::steady_clock clk;

int main() {
    const int SIZE = 8192;
    int i;
    clk::time_point t0, t1;

    const char* version = openblas_get_config();
    int num_threads = openblas_get_num_threads();
    std::cout << version << "\n";
    std::cout << "using " << num_threads << " threads\n";
    std::cout << "on " << openblas_get_corename() << "\n";

    std::cout << std::fixed << std::setprecision(1);

    std::cout << SIZE << "x" << SIZE << " matrix multiplication\n";
    std::cout << "tick = " << clk::period::num << "/" << clk::period::den << " s\n";
    double tick = (double) clk::period::num / (double) clk::period::den;


    float *A = (float *) malloc(sizeof(float) * SIZE * SIZE);
    float *B = (float *) malloc(sizeof(float) * SIZE * SIZE);
    float *C = (float *) malloc(sizeof(float) * SIZE * SIZE);
    for (i = 0; i < SIZE * SIZE; i++) {
        A[i] = ((float) rand()) / RAND_MAX;
        B[i] = ((float) rand()) / RAND_MAX;
    }

    enum CBLAS_ORDER order = CblasRowMajor;
    enum CBLAS_TRANSPOSE transA = CblasNoTrans;
    enum CBLAS_TRANSPOSE transB = CblasNoTrans;

    for (i = 0; i < 10; i++) {

        t0 = clk::now();

        cblas_sgemm(order, transA, transB, SIZE, SIZE, SIZE, 1.0,
                    A, SIZE, B, SIZE, 0.0, C, SIZE);

        t1 = clk::now();

        std::cout << std::setw(8)
             << (double) (2 * (double) SIZE * SIZE * SIZE - (double) SIZE * SIZE) /
                (double) (t1 - t0).count() * tick * 1.0E9 
             << " GFLOPS fp32\n";
    }

    return 0;
}

